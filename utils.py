from copy import deepcopy
from urllib.parse import urlparse

from PIL import Image


def uri_validator(x):
    try:
        result = urlparse(x)
        return all([result.scheme, result.netloc, result.path])
    except:
        return False


def resize_image(image, max_size: int = 12000):
    img = Image.open(deepcopy(image))
    if sum(img.size) > max_size:
        wpercent = (2000 / float(img.size[0]))
        hsize = int((float(img.size[1]) * float(wpercent)))
        img = img.resize((2000, hsize), Image.LANCZOS)
        img.save(image, format='jpeg')
    img.close()
    return image
