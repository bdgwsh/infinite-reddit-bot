import logging
import os
import random
import time
from datetime import datetime
from logging.config import dictConfig

from api.reddit import RedditBot
from settings import subreddit_vkgroups, logger

dictConfig(logger)

logging.info("Start InfiniteRedditBot")
logging.info('*' * 30)
logging.info("SetUp subreddits:")
for k, v in subreddit_vkgroups.items():
    logging.info(f"{k}:\thttps://reddit.com/r/{k}   config: {v}")

sl = 1
logging.info("=" * 30)
for subreddit, config in subreddit_vkgroups.items():
    logging.info(f"Sleep {sl} seconds")
    time.sleep(sl)
    logging.info('-' * 30)
    logging.info(f"working on: {subreddit}")
    r = RedditBot(subreddit, config)
    if datetime.now().isoweekday() == 6:
        if datetime.now().hour == 19:
            logging.info(f"today is saturday and 19 hours, avatar update")
    r.avatar_update()

    c = 4
    res = None
    after = None

    while not res and c > 0:
        c -= 1
        try:
            after, res = r.post_hot(after=after)
        except Exception as e:
            logging.exception(e)
        logging.info(f"status: {res}, after: {after}, c={c}")
        time.sleep(1)

    sl = random.randint(1, 30)

logging.info("Stop InfiniteRedditBot")
logging.info('*' * 30)
logging.info('\n')
