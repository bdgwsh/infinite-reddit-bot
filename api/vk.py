from enum import Enum
from io import BytesIO
from typing import Union, List

import requests
import vk_api
from PIL import Image

from utils import uri_validator, resize_image

Image.MAX_IMAGE_PIXELS = None


class FileType(Enum):
    doc = 'doc'
    image = 'image'
    video = 'video'


class Attachment:
    def __init__(self, name, content, file_type: FileType):
        self.name = name
        self.content = content
        self.type = file_type
        self.id = None


class Post:
    def __init__(self, token: str, group_id: Union[str, int]):
        vk_session = vk_api.VkApi(token=token)
        self.vk = vk_session.get_api()
        self.vk_upload = vk_api.VkUpload(self.vk)
        self.group_id = group_id
        self.attachments = []

    def create(self, message: str, attachments: Union[List[Attachment], Attachment], source: str = ''):
        self.upload_attachments(attachments if isinstance(attachments, list) else [attachments])
        response = self.vk.wall.post(owner_id=f"-{self.group_id}", from_group=1, message=message,
                                     attachment=','.join(self.attachments), copyright=source)
        return response['post_id']

    def upload_attachments(self, attachments: List[Attachment]):
        photos = []
        for attach in attachments:
            if attach.type is FileType.image:
                photos.append(self._resize_image(attach))
            elif attach.type is FileType.doc:
                self._upload_doc(attach)
            elif attach.type is FileType.video:
                self._upload_video(attach)
        if photos:
            self._upload_photo(photos)

    def _resize_image(self, image: Attachment):
        content = image.content
        if isinstance(content, bytes):
            image.content = resize_image(BytesIO(content))
        else:
            image.content = resize_image(content)
        return image

    def _upload_video(self, video: Attachment):
        if uri_validator(video.content):
            response = self.vk_upload.video(link=video.content, name=video.name, group_id=self.group_id)
        elif isinstance(video.content, (bytes, str)):
            file = video.content if isinstance(video.content, str) else BytesIO(video.content)
            response = self.vk_upload.video(video_file=file, name=video.name, group_id=self.group_id)
        else:
            return
        if response:
            self.attachments.append(f"video{response['owner_id']}_{response['video_id']}")

    def _upload_photo(self, image: Union[List[Attachment], Attachment]):
        response = self.vk_upload.photo_wall([x.content for x in image], group_id=self.group_id)
        self.attachments.extend([f"photo{x['owner_id']}_{x['id']}" for x in response])

    def _upload_doc(self, doc: Attachment):
        response = self.vk_upload.document_wall(doc.content, title=doc.name, group_id=self.group_id)
        self.attachments.append(f"doc{response['owner_id']}_{response['id']}")


def wall_pin(token, post_id, owner_id):
    params = {'access_token': token, 'post_id': post_id, 'owner_id': owner_id, 'v': '5.101'}
    response = requests.post("https://api.vk.com/method/wall.pin", data=params).json()
    return response['response']


def status_update(token, group_id, text):
    params = {'access_token': token, 'group_id': group_id, 'text': text, 'v': '5.101'}
    response = requests.post("https://api.vk.com/method/status.set", data=params).json()
    return response['response']


def avatar_update(token, owner_id, content):
    params = {'access_token': token, 'owner_id': owner_id, 'v': '5.101'}
    response = requests.post("https://api.vk.com/method/photos.getOwnerPhotoUploadServer", data=params).json()
    response = requests.post(response['response']['upload_url'],
                             files={'photo': ('avatar.jpg', BytesIO(content))}).json()
    response = requests.post("https://api.vk.com/method/photos.saveOwnerPhoto", data={**params, **response}).json()
    return response

# class Doc:
#     def __init__(self, token, group_id, doc):
#         self.doc = doc
#         self.group_id = group_id
#         self.default_params = {'access_token': token, 'v': '5.130'}
#
#     def upload(self, name):
#         if isinstance(self.doc, bytes):
#             doc = self.__upload(self.doc, name)
#         else:
#             with open(self.doc, mode="rb") as data:
#                 content = data.read()
#             doc = self.__upload(content, name)
#         return doc
#
#     def __upload(self, content, name):
#         params = {'group_id': self.group_id, **self.default_params}
#         response = requests.post("https://api.vk.com/method/docs.getUploadServer", data=params).json()
#         upload_url = response['response']['upload_url']
#         response = requests.post(upload_url, files={'file': (name, content)}).json()
#         params = {"file": response['file'], **self.default_params}
#         response = requests.post("https://api.vk.com/method/docs.save", data=params).json()
#         return f"doc{response['response'][0]['owner_id']}_{response['response'][0]['id']}"
#
#
# class Image:
#     def __init__(self, token, group_id, image):
#         self.image = image
#         self.group_id = group_id
#         self.default_params = {'access_token': token, 'v': '5.130'}
#
#     def upload(self, name):
#         if isinstance(self.image, bytes):
#             content = self.resize_image(BytesIO(self.image))
#         else:
#             image = self.resize_image(self.image)
#             with open(image, mode="rb") as data:
#                 content = data.read()
#         photo = self._upload_image(content, name)
#         return photo
#
#     def _upload_image(self, content, name):
#         params = {'group_id': self.group_id, **self.default_params}
#         response = requests.post("https://api.vk.com/method/photos.getWallUploadServer", data=params).json()
#         upload_url = response['response']['upload_url']
#         response = requests.post(upload_url, files={'photo': (name, content)}).json()
#         response = requests.post("https://api.vk.com/method/photos.saveWallPhoto", data={**params, **response}).json()
#         return f"photo{response['response'][0]['owner_id']}_{response['response'][0]['id']}"
#
#
# class Video:
#     def __init__(self, token, group_id, video, album_id=''):
#         self.video = video
#         self.group_id = group_id
#         self.album_id = album_id
#         self.default_params = {'access_token': token, 'v': '5.130'}
#
#     def upload(self, name):
#         if isinstance(self.video, bytes):
#             video = self.__upload(self.video, name)
#         elif uri_validator(self.video):
#             video = self.video_save()
#         else:
#             with open(self.video, mode="rb") as data:
#                 content = data.read()
#             doc = self.__upload(content, name)
#         return doc
#
#     def __upload(self, content, name):
#         params = {'group_id': self.group_id, **self.default_params}
#         response = requests.post("https://api.vk.com/method/docs.getUploadServer", data=params).json()
#         upload_url = response['response']['upload_url']
#         response = requests.post(upload_url, files={'file': (name, content)}).json()
#         params = {"file": response['file'], **self.default_params}
#         response = requests.post("https://api.vk.com/method/docs.save", data=params).json()
#         return f"doc{response['response'][0]['owner_id']}_{response['response'][0]['id']}"
#
#     def video_save(self, album_id, link):
#         params = self.params.copy()
#         params.update({"album_id": album_id, "owner_id": f"-{self.group_id}"})
#         response = requests.post("https://api.vk.com/method/video.getAlbumById", data=params).json()
#         count = response.get('response').get('count')
#         if count >= 1000:
#             raise Exception('Album is full')
#         else:
#             if 'vk.com' in link:
#                 video = os.path.split(link)[1].replace('video', '').split('_')  # улучшить используя регулярки, позже
#                 params.update({"target_id": f"-{self.group_id}", "album_id": album_id,
#                                "owner_id": video[0], "video_id": video[1]})
#                 response = requests.post("https://api.vk.com/method/video.addToAlbum", data=params).json()
#                 if response['response'] == 1:
#                     return f"video{video[0]}_{video[1]}"
#                 else:
#                     return None
#             else:
#                 params.update({"group_id": f"{self.group_id}", "album_id": album_id, "link": link})
#                 response = requests.post("https://api.vk.com/method/video.save", data=params).json()
#                 requests.get(response['response']['upload_url'])
#                 return f"video{response['response']['owner_id']}_{response['response']['video_id']}"


# class Posting:
#     def __init__(self, token, group_id):
#         self.token = token
#         self.group_id = group_id
#         self.params = {'access_token': token, 'v': '5.101'}
#
#     def create_post(self, message: str, attachments: list, source: str = ''):
#         attachments = self.upload_attachments(attachments)
#         logging.debug(attachments)
#
#         params = self.params.copy()
#         params.update({"owner_id": f"-{self.group_id}",
#                        "from_group": 1, 'message': message,
#                        'attachment': ','.join(attachments),
#                        'copyright': str(source)})
#         response = requests.post("https://api.vk.com/method/wall.post", data=params).json()
#         post_id = response['response']['post_id']
#
#         params = self.params.copy()
#         params.update({'posts': f"-{self.group_id}_{post_id}"})
#         post = requests.post("https://api.vk.com/method/wall.getById", data=params).json()
#         if attachments:
#             if ('attachments' not in post['response'][0]) or \
#                     ('attachments' in post['response'][0] and len(post['response'][0]['attachments']) < len(
#                         attachments)):
#                 message = post['response'][0]['text']
#                 message += "\n\n___________"
#                 params = self.params.copy()
#                 params.update({'owner_id': f"-{self.group_id}", 'post_id': post_id, 'message': message,
#                                'attachments': ','.join(attachments)})
#                 requests.post("https://api.vk.com/method/wall.edit", data=params)
#
#             if 'attachments' in post['response'][0]:
#                 params = self.params.copy()
#
#                 for at in post['response'][0]['attachments']:
#                     if 'video' not in at:
#                         continue
#                     if at['video']['duration'] == 0 and at['video']['title'] == 'Без названия':
#                         if len(post['response'][0]['attachments']) > 1:
#                             try:
#                                 params.update({"owner_id": f"-{self.group_id}",
#                                                'message': message, 'attachment': ','.join(attachments)})
#                                 attachments.remove(f"video{at.get('owner_id')}_{at.get('id')}")
#                             except ValueError:
#                                 pass
#                         else:
#                             params.update({'owner_id': f"-{self.group_id}", 'post_id': post_id})
#                             requests.post("https://api.vk.com/method/wall.delete", data=params)
#         return post_id
#
#     def upload_attachments(self, attachments):
#         uploaded_attachments = []
#         upload_status = {}
#         for attach in attachments:
#             upload_status[attach] = False
#             if attach.type.value == 'image':
#                 image_uploader = ImageUploader(self.token, self.group_id, attach.content)
#                 attach = image_uploader.upload(attach.name)
#             elif attach.type.value == 'doc':
#                 doc_uploader = DocUploader(self.token, self.group_id, attach.content)
#                 attach = doc_uploader.upload(attach.name)
#             elif attach.type.value == 'video':
#                 attach = self.video_save(attach.album_id, attach.content)
#             if attach:
#                 upload_status[attach] = True
#                 uploaded_attachments.append(attach)
#             time.sleep(1)
#         return uploaded_attachments
#
#     def video_save(self, album_id, link):
#         params = self.params.copy()
#         params.update({"album_id": album_id, "owner_id": f"-{self.group_id}"})
#         response = requests.post("https://api.vk.com/method/video.getAlbumById", data=params).json()
#         count = response.get('response').get('count')
#         if count >= 1000:
#             raise Exception('Album is full')
#         else:
#             if 'vk.com' in link:
#                 video = os.path.split(link)[1].replace('video', '').split('_')  # улучшить используя регулярки, позже
#                 params.update({"target_id": f"-{self.group_id}", "album_id": album_id,
#                                "owner_id": video[0], "video_id": video[1]})
#                 response = requests.post("https://api.vk.com/method/video.addToAlbum", data=params).json()
#                 if response['response'] == 1:
#                     return f"video{video[0]}_{video[1]}"
#                 else:
#                     return None
#             else:
#                 params.update({"group_id": f"{self.group_id}", "album_id": album_id, "link": link})
#                 response = requests.post("https://api.vk.com/method/video.save", data=params).json()
#                 requests.get(response['response']['upload_url'])
#                 return f"video{response['response']['owner_id']}_{response['response']['video_id']}"
